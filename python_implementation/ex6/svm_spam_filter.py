import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.io import loadmat
from sklearn import svm

spam_train = loadmat('data/spamTrain.mat')
spam_test = loadmat('data/spamTest.mat')

X = spam_train['X']
X_test = spam_test['Xtest']
y = spam_train['y'].ravel()
y_test = spam_test['ytest'].ravel()

# use grid search to find optimal values for classification in spam filter
C_values = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30, 100]
gamma_values = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30, 100]

best_score = 0
best_params = {'C': None, 'gamma': None}

for C in C_values:
    for gamma in gamma_values:
        svc = svm.SVC(C=C, gamma=gamma)
        svc.fit(X,y)
        score = svc.score(X_test, y_test) # pretend this is a validation set

        print('C = {0}, gamma = {1}, accuracy = {2:.2f}%'.format(C, gamma, 100*score))
        
        if score > best_score:
            best_score = score
            best_params['C'] = C
            best_params['gamma'] = gamma

print('\nFound best params, C = {0}, gamma = {1}, with spam classification accuracy {2:.2f}%'.format(best_params['C'], best_params['gamma'], 100 * best_score))
