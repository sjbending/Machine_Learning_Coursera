import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb

from scipy.io import loadmat
from sklearn import svm

def runSVM(data, C_val, num_iter):
    # feeling lazy today so use the support vector machine from sklearn
    svc = svm.LinearSVC(C=C_val, loss='hinge', max_iter=num_iter) # SVC = support vector classifier, hinge = standard SVM loss

    # fit it to the data
    svc.fit(data[['X1', 'X2']], data['y'])

    # check on the accuracy
    print('\nAccuracy of SVM with C = {0} is: {1:.2f}%'.format(C_val, 100*svc.score(data[['X1', 'X2']], data['y'])))

    return svc

raw_data = loadmat('data/ex6data1.mat')

data = pd.DataFrame(raw_data['X'], columns=['X1', 'X2'])
data['y'] = raw_data['y']

# plot the data
positive = data[data['y'].isin([1])]
negative = data[data['y'].isin([0])]

fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(positive['X1'], positive['X2'], s=50, marker='x', label='Positive')
ax.scatter(negative['X1'], negative['X2'], s=50, marker='o', label='Negative')
ax.legend()
plt.show()

# initialise params to use
num_iter=200000

# Try SVM with C = 1
svc1 = runSVM(data, 1, num_iter)

# Try SVM with C = 500
svc2 = runSVM(data, 500, num_iter)

# Visualise decision boundary
data['SVM 1 Confidence'] = svc1.decision_function(data[['X1', 'X2']])

fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(data['X1'], data['X2'], s=50, c=data['SVM 1 Confidence'], cmap='seismic')
ax.set_title('SVM (C=1) Decision Confidence')

# do the same for SVM 2
data['SVM 2 Confidence'] = svc2.decision_function(data[['X1', 'X2']])

fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(data['X1'], data['X2'], s=50, c=data['SVM 2 Confidence'], cmap='seismic')
ax.set_title('SVM (C=1) Decision Confidence')
plt.show()
