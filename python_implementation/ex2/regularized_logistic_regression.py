import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize as opt

# activation function for logistic regression
# converts continuous input into value between 0 and 1
# interpreted as likelihood input sample should be classified positively
def sigmoid(z):
    return 1 / (1 + np.exp(-z))

# fmin_tnc expects first argument to be theta
def costFunction(theta, X, y, Lambda):
    m = len(X)
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)
    
    h = sigmoid(X * theta.T)
    
    first  = np.multiply(-y, np.log(h))
    second = np.multiply((1-y), np.log(1 - h))
    cost = (1/m) * np.sum(first - second)

    regularisation = (Lambda / (2*m)) * np.sum(np.square(theta[:,1:]))

    return cost + regularisation

def gradient(theta, X, y, Lambda):
    m = len(X)
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)

    h = sigmoid(X * theta.T)
    gradient = (1/m) * np.dot((h - y).T, X)
    
    regularisation = (Lambda / m) * theta[:,1:]
    
    return gradient + np.hstack((np.matrix(0), regularisation))


def predict(theta, X):
    probability = sigmoid(X * theta.T)
    return [1 if x >= 0.5 else 0 for x in probability]

path = os.getcwd() + '/data/ex2data2.txt'
data = pd.read_csv(path, header=None, names=['Test 1', 'Test 2', 'Admitted'])

# have a quick look at the data
print('\nFirst few data points:')
print(data.head())

# plot data, first divide data by admitted or not
positive = data[data['Admitted'].isin([1])]
negative = data[data['Admitted'].isin([0])]

# draw with different colours for positive and negative
fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(positive['Test 1'], positive['Test 2'], s=50, c='b', marker='o', label='Admitted') # note: s = marker size
ax.scatter(negative['Test 1'], negative['Test 2'], s=50, c='r', marker='o', label='Not Admitted')
ax.legend()
ax.set_xlabel('Test 1 Score')
ax.set_ylabel('Test 2 Score')
plt.show()

# use a polynomial to fit this data
degree = 5
x1 = data['Test 1']
x2 = data['Test 2']

# create range of polynomial terms with power i-j for x1 and j for x2
for i in range(1, degree):
    for j in range(0, i):
        data['F' + str(i) + str(j)] = np.power(x1, i-j) * np.power(x2, j)

data.drop('Test 1', axis=1, inplace=True)
data.drop('Test 2', axis=1, inplace=True)

print(data.head())

# divide data into training data, X, and target variable, y
cols = data.shape[1]
X = data.iloc[:,1:cols]
y = data.iloc[:,0:1]

# normalise the features
X = (X - X.mean()) / X.std()

# insert x0
X.insert(0, 'x0', 1)

# convert to numpy arrays and initialise theta
X = np.array(X.values)
y = np.array(y.values)
theta = np.zeros(cols)

# learning rate
Lambda = 0

# use scipy fmintnc to perform the minimisation (gradient descent)
result = opt.fmin_tnc(func=costFunction, x0=theta, fprime=gradient, args=(X,y,Lambda))

print(result)

# score the training accuracy of this classifier
theta_min = np.matrix(result[0])
predictions = predict(theta_min, X)
correct = [1 if ((a == 1 and b == 1) or (a == 0 and b == 0 )) else 0 for (a, b) in zip(predictions, y)]
accuracy = sum(correct) / len(correct)
print('\nTraining accuracy = {0}%'.format(100*accuracy))
