import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.io import loadmat
from scipy import ndimage

# principal component analysis to get principal components of matrix
def pca(X):
    # normalise features
    X = (X - X.mean()) / X.std()

    # compute covariance matrix
    X = np.matrix(X)
    cov = (X.T * X) / X.shape[0]

    # singular value decomposition to get eigenvectors
    U, S, V = np.linalg.svd(cov)

    return U, S, V

# project original data onto a lower-dimensional space and select the top k components
def projectData(X, U, k):
    U_reduced = U[:,:k]
    return np.dot(X, U_reduced)

# reverse steps in project data to attempt to recover original data
def recoverData(Z, U, k):
    U_reduced = U[:,:k]
    return np.dot(Z, U_reduced.T)

data = loadmat('data/ex7data1.mat')
X = data['X']

# get principal components (matrix U)
U, S, V = pca(X)

# project data onto one dimension
num_dim = 1
Z = projectData(X, U, num_dim)

# attempt to recover original data
X_recovered = np.array(recoverData(Z, U, num_dim))

# plot the original data vs. the recovered data
fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(X[:,0],X[:,1], s=30, color='b', label='Original Data')
ax.scatter(X_recovered[:,0],X_recovered[:,1], s=30, color='tab:orange', label='Recovered Data')
plt.legend()
plt.show()

# next apply PCA to images of faces to capture the 'essence' of the images
faces = loadmat('data/ex7faces.mat')
X = faces['X']

# let's look at a face
facenum = 100
face = np.reshape(X[100,:], (32, 32))
rotated_face = ndimage.rotate(face, 270)
plt.imshow(rotated_face)

# run principal component analysis on our faces
U, S, V = pca(X)

# project it to a lower dimensionality
num_dims = 25
Z = projectData(X, U, num_dims)

# attempt to recover original structure
X_recovered = recoverData(Z, U, num_dims)
face = np.reshape(X_recovered[facenum,:], (32, 32))
rotated_face = ndimage.rotate(face, 270)
fig, ax = plt.subplots(figsize=(6.2,4.8))
plt.imshow(rotated_face)
plt.show()

eigvals = 100 * (S**2 / np.sum(S**2))

fig = plt.figure(figsize=(8,5))
sing_vals = np.arange(10)
plt.plot(sing_vals, eigvals[:10], 'ro-', linewidth=2)
plt.xlabel('Principle Component')
plt.ylabel('Percentage Explained Variance (%)')
plt.show()
