import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.io import loadmat
from scipy.optimize import minimize

epsilon = np.finfo(float).eps

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def costFunction(theta, X, y, Lambda):
    m = len(X)
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)

    h = sigmoid(X * theta.T)
    first = np.multiply(-y, np.log(h))
    second = np.multiply((1 - y), np.log(1 - h + epsilon))
    cost = np.sum(first - second) / m
    regularisation = (Lambda / (2*m)) * np.sum(np.square(theta[:,1:]))

    return cost + regularisation

def gradient(theta, X, y, Lambda):
    m = len(X)
    theta = np.matrix(theta)
    X = np.matrix(X)
    y = np.matrix(y)

    h = sigmoid(X * theta.T)
    gradient = (1/m) * np.dot((h - y).T, X)
    regularisation = (Lambda / m) * theta[:,1:]
    
    return gradient + np.hstack((np.matrix(0), regularisation))

def one_vs_all(X, y, num_labels, Lambda):
    num_rows, num_params = X.shape

    # k * (n + 1) array for the params of each of the k classifiers
    all_theta = np.zeros((num_labels, num_params + 1))

    # insert column of 1s for intercept term
    X = np.insert(X, 0, values=np.ones(num_rows), axis=1)
    
    # labels are 1-indexed instead of 0-indexed
    for i in range(1, num_labels + 1):
        theta = np.zeros((num_params + 1, 1))

        # minimise the regression function
        fmin = minimize(fun=costFunction, x0=theta, args=(X, 1*(y == i), Lambda), method='TNC', jac=gradient) # note: jac = jacobian, TNC = truncated newton method
        all_theta[i-1,:] = fmin.x

    return all_theta

def predict_all(X, all_theta):
    num_rows = X.shape[0]
    num_params = X.shape[1]
    num_labels = all_theta.shape[0]

    # same as before, insert ones to match shape
    X = np.insert(X, 0, values=np.ones(num_rows), axis=1)

    # convert to matrices
    X = np.matrix(X)
    all_theta = np.matrix(all_theta)

    # compute class probability for each class on each training instance
    h = sigmoid(X * all_theta.T)

    # create array of the index with max probability
    h_argmax = np.argmax(h, axis=1)

    # since array zero indexed need to add one for true label prediction
    h_argmax = h_argmax + 1

    return h_argmax

# get our data
data = loadmat('data/ex3data1.mat')
print(data)

rows = data['X'].shape[0]
params = data['X'].shape[1]

all_theta = np.zeros((10, params + 1))

X = np.insert(data['X'], 0, values=np.ones(rows), axis=1)

theta = np.zeros(params + 1)

y_0 = np.array([1 if label == 0 else 0 for label in data['y']])
y_0 = np.reshape(y_0, (rows, 1))

print(X.shape, y_0.shape, theta.shape, all_theta.shape)

all_theta = one_vs_all(data['X'], data['y'], len(np.unique(data['y'])), 1)

y_pred = predict_all(data['X'], all_theta)
correct = [1 if a == b else 0 for (a, b) in zip(y_pred, data['y'])]
accuracy = (sum(map(int, correct)) / float(len(correct)))
print('accuracy = {0}%'.format(accuracy * 100))
