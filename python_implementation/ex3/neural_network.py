import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from scipy.io import loadmat
from sklearn.preprocessing import OneHotEncoder
from scipy.optimize import minimize
from sklearn.metrics import accuracy_score

eps = np.finfo(float).eps

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def forward_propagate(X, theta1, theta2):
    m = X.shape[0]

    a1 = np.insert(X, 0, values = np.ones(m), axis=1) # (5000, 401)
    z2 = a1 * theta1.T # (5000, 25)
    a2 = np.insert(sigmoid(z2), 0, values = np.ones(m), axis=1) # (5000, 26)
    z3 = a2 * theta2.T # (5000, 10)
    h = sigmoid(z3) # hypothesis vector, (5000, 10)

    return a1, z2, a2, z3, h

def costFunction(params, input_size, hidden_size, num_labels, X, y, Lambda, info):
    m = X.shape[0]
    X = np.matrix(X)
    y = np.matrix(y)

    # reshape parameter array into parameter matrices for each layer
    theta1 = np.matrix(np.reshape(params[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1)))) # (25, 401)
    theta2 = np.matrix(np.reshape(params[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1)))) # (10, 26)

    # run the forward propagation pass
    a1, z2, a2, z3, h = forward_propagate(X, theta1, theta2)

    # compute the cost
    J = 0
    for i in range(m):
        first_term = np.multiply(-y[i,:], np.log(h[i,:]))
        second_term = np.multiply((1 - y[i,:]), np.log(1 - h[i,:] + eps))
        J += np.sum(first_term - second_term)

    # regularisation
    J = J/m + (Lambda / (2*m)) * (np.sum(np.square(theta1[:,1:])) + np.sum(np.square(theta2[:,1:])))

    if info['Nfeval']%10 == 0:
        print('{0:4d}     {1:9f}'.format(info['Nfeval'], J))
    info['Nfeval'] += 1
    
    return J

def sigmoid_gradient(z):
    return np.multiply(sigmoid(z), (1 - sigmoid(z)))
# unravel parameter array into matrices for each layer

def backpropagation(params, input_size, hidden_size, num_labels, X, y, Lambda, info):
    m = X.shape[0]
    X = np.matrix(X)
    y = np.matrix(y)

    # unravel parameter array into matrices for each layer
    theta1 = np.matrix(np.reshape(params[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1)))) # (25, 401)
    theta2 = np.matrix(np.reshape(params[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1)))) # (10, 26)

    # run forward pass
    a1, z2, a2, z3, h = forward_propagate(X, theta1, theta2)

    # initialise
    delta1 = np.zeros(theta1.shape) # (25, 401)
    delta2 = np.zeros(theta2.shape) # (10, 26)

    # perform backpropagation
    for t in range(m):
        a1t = a1[t,:] # (1, 401)
        z2t = z2[t,:] # (1, 25)
        a2t = a2[t,:] # (1, 26)
        ht = h[t,:] # (1, 10) -- hypothesis
        yt = y[t,:] # (1, 10) -- target

        d3t = ht - yt # (1, 10) -- error

        z2t = np.insert(z2t, 0, values=np.ones(1)) # (1, 26)
        d2t = np.multiply((theta2.T * d3t.T).T, sigmoid_gradient(z2t)) # (1, 26)

        delta1 = delta1 + (d2t[:,1:]).T * a1t
        delta2 = delta2 + d3t.T * a2t

    delta1 = delta1 / m
    delta2 = delta2 / m

    # regularisation
    delta1[:,1:] = delta1[:,1:] + (theta1[:,1:] * Lambda) / m
    delta2[:,1:] = delta2[:,1:] + (theta2[:,1:] * Lambda) / m

    # unravel gradient matrices into single array
    gradient = np.concatenate((np.ravel(delta1), np.ravel(delta2)))
    
    return gradient
        
# load up data
data = loadmat('data/ex3data1.mat')

# divide into features and target variables
X = data['X']
y = data['y']

# one hot encode the target variable
encoder = OneHotEncoder(sparse=False)
y_onehot = encoder.fit_transform(y)

# initial setup
input_size = 400
hidden_size = 25
num_labels = 10
Lambda = 1
num_iter = 250

# randomly initialise parameter array of the size of the full network's parameters
params = (np.random.random(size=hidden_size * (input_size +1) + num_labels * (hidden_size + 1)) - 0.5) / 4

# print for tracking how it's doing
print('{0:4s}     {1:9s}'.format('Iter', 'Cost function'))

# minimise the objective function
fmin = minimize(fun=costFunction, x0=params, args=(input_size, hidden_size, num_labels, X, y_onehot, Lambda, {'Nfeval':0}), method='TNC', jac=backpropagation, options={'maxiter': num_iter})

# acquire our minimised theta1 and theta2
X = np.matrix(X)
theta1 = np.matrix(np.reshape(fmin.x[:hidden_size * (input_size + 1)], (hidden_size, (input_size + 1))))
theta2 = np.matrix(np.reshape(fmin.x[hidden_size * (input_size + 1):], (num_labels, (hidden_size + 1))))

# forward propagate our data with them
a1, z2, a2, z3, h = forward_propagate(X, theta1, theta2)

# acquire the predictions
y_pred = np.array(np.argmax(h, axis=1) + 1) # +1 because of the 0 indexing

# check the accuracy
accuracy = accuracy_score(y_pred, y)
print('accuracy = {0}%'.format(accuracy * 100))
