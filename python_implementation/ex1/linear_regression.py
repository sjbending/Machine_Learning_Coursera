import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def computeCost(X, y, theta):
    m = len(X)
    prediction = np.dot(X, theta.T)
    error = np.subtract(np.dot(X, theta.T), y)
    cost = np.sum(np.square(error)) / (2 * m)
    gradient = np.dot(error.T, X) / m
    return cost, gradient

def gradientDescent(X, y, theta, alpha, num_iters):
    cost = np.zeros(num_iters) # empty matrix to store costs
    for i in range(num_iters):
        cost[i], grad = computeCost(X, y, theta)
        theta = theta - grad * alpha

    return theta, cost

# acquire data (read_csv works for txt files too)
path = os.getcwd() + '/data/ex1data1.txt' # cwd() = current working directory
data = pd.read_csv(path, header=None, names=['Population', 'Profit'])

# insert column of ones for theta0
data.insert(0, 'x0', 1)

# split data into X (training data) and y (target variable)
cols = data.shape[1]
X = data.iloc[:, 0:cols-1]
y = data.iloc[:, cols-1:cols]

# convert from dataframe to numpy matrices and init param matrix
X = np.matrix(X.values)
y = np.matrix(y.values)
theta = np.matrix(np.random.random(X.shape[1]))

# initialise variables for learning rate and iterations
alpha = 0.01
num_iters = 50

# perform gradient descent to fit the model parameters
theta, cost = gradientDescent(X, y, theta, alpha, num_iters)

# view results
x = np.linspace(data.Population.min(), data.Population.max(), 100)
f = theta[0, 0] + (theta[0, 1] * x) # function to plot

fig, ax = plt.subplots(figsize=(12,8))
ax.plot(x, f, 'r', label='Prediction')
ax.scatter(data.Population, data.Profit, label='Training Data')
ax.legend(loc=2) # loc = legend location
ax.set_xlabel('Population')
ax.set_ylabel('Profit')
ax.set_title('Predicted Profit vs. Population Size')
plt.show()

# view cost as a function of the number of iterations
fig, ax = plt.subplots(figsize=(12,8))
ax.plot(np.arange(num_iters), cost, 'r') # arange = evenly spaced range
ax.set_xlabel('Num Iterations')
ax.set_ylabel('Cost')
ax.set_title('Error vs. Training Epoch')
plt.show()
