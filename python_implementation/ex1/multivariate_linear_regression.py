import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

# acquire data (read_csv works for txt files too)
path = os.getcwd() + '/data/ex1data2.txt' # cwd() = current working directory
data = pd.read_csv(path, header=None, names=['Size', 'Bedrooms', 'Price'])

# take a look at data
print("\nFirst few data points:")
print(data.head())

# normalise features
data = (data - data.mean()) / data.std()

# insert column of ones for theta0
data.insert(0, 'x0', 1)

# split data into X (training data) and y (target variable)
cols = data.shape[1]
X = data.iloc[:, 0:cols-1]
y = data.iloc[:, cols-1:cols]

# convert from dataframe to numpy matrices
X = np.matrix(X.values)
y = np.matrix(y.values)

# can just use sklearn library to do simple linear regression
# note that this doesn't actually use gradient descent since it wouldn't be very efficient
model = linear_model.LinearRegression()
model.fit(X, y)

prediction = model.predict(X)

# model prediction
rmse = mean_squared_error(y, prediction)
r2 = r2_score(y, prediction)

print('\nSlope: ', model.coef_)
print('Intercept: ', model.intercept_)
print('Root mean squared error: ', rmse)
print('R2 score: ', r2)
