import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

path = os.getcwd() + '/data/ex1data1.txt' # cwd() = get current working directory
data = pd.read_csv(path, header=None, names=['Population', 'Profit'])

# display first few rows
print("\n First few data points:")
print(data.head())

# display basic statistics with describe
print("\n Basic statistics for dataset:")
print(data.describe())

# plot data in scatter plot
data.plot(kind='scatter', x='Population', y='Profit', figsize=(12,8))
plt.show()
