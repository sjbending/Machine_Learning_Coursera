import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb

from scipy.io import loadmat
from scipy import stats

def estimateGaussian(X):
    mu = X.mean(axis=0)
    sigma = X.var(axis=0)

    return mu, sigma

# function to find best threshold value given prob density values + true labels
def selectedThreshold(pval, yval):
    best_epsilon = 0
    best_f1 = 0
    f1 = 0

    step = (pval.max() - pval.min()) / 1000

    for epsilon in np.arange(pval.min() + step, pval.max(), step):
        predictions = pval < epsilon

        # true positive
        tp = np.sum(np.logical_and(predictions == 1, yval == 1)).astype(float)
        # false positive
        fp = np.sum(np.logical_and(predictions == 1, yval == 0)).astype(float)
        #false negative
        fn = np.sum(np.logical_and(predictions == 0, yval == 1)).astype(float)
        
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        f1 = (2 * precision * recall) / (precision + recall)

        if f1 > best_f1:
            best_f1 = f1
            best_epsilon = epsilon

        return best_epsilon, best_f1
    
# load up our data
data = loadmat('data/ex8data1.mat')
X = data['X']

# plot our data
fig, ax = plt.subplots(figsize=(12,8))
ax.scatter(X[:,0], X[:,1])
plt.show()

# get the mean and variance
mu, sigma = estimateGaussian(X)

# use validation data to determine probability threshold
Xval = data['Xval']
yval = data['yval']

# calculate probability data point belongs to normal distribution
dist = stats.norm(mu[0], sigma[0]) # generate normal distribution
dist.pdf(X[:,0])[0:50] # calculate probability first 50 instances belong to it

# compute and save probability density of each value in dataset
p = np.zeros((X.shape[0], X.shape[1]))
p[:,0] = stats.norm(mu[0], sigma[0]).pdf(X[:,0])
p[:,1] = stats.norm(mu[1], sigma[1]).pdf(X[:,1])

# do the same for validations et
pval = np.zeros((Xval.shape[0], Xval.shape[1]))
pval[:,0] = stats.norm(mu[0], sigma[0]).pdf(Xval[:,0])
pval[:,1] = stats.norm(mu[1], sigma[1]).pdf(Xval[:,1])

# find threshold
epsilon, f1 = selectedThreshold(pval, yval)

print(epsilon)
print(f1)

# apply threshold to dataset
outliers = np.where(p < epsilon)

fix, ax = plt.subplots(figsize=(12,8))
ax.scatter(X[:,0], X[:,1])
ax.scatter(X[outliers[0],0], X[outliers[0],1], s=50, color='r', marker='o')
plt.show()
