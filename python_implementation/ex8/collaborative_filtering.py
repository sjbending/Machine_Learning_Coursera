import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb

from scipy.io import loadmat
from scipy.optimize import minimize

def costFunction(params, Y, R, num_features, Lambda):
    Y = np.matrix(Y)
    R = np.matrix(R)
    num_movies = Y.shape[0]
    num_users = Y.shape[1]

    # reshape the parameter array into matrices
    X = np.matrix(np.reshape(params[:num_movies * num_features], (num_movies, num_features))) # (1682, 10)
    Theta = np.matrix(np.reshape(params[num_movies * num_features:], (num_users, num_features))) # (943, 10)

    # initialize outputs
    J = 0
    X_grad = np.zeros(X.shape) # (1682, 10)
    Theta_grad = np.zeros(Theta.shape) # (943, 10)
    
    # compute cost
    error = np.multiply((X * Theta.T) - Y, R) # (1682, 943)
    squared_error = np.square(error) # (1682, 943)
    J = (1 / 2) * np.sum(squared_error)

    # regularise the cost
    J = J + (Lambda / 2) * (np.sum(np.square(Theta)) + np.sum(np.square(X)))
    
    # calculate the gradients with regularisation
    X_grad = error * Theta + (Lambda * X)
    Theta_grad = error.T * X + (Lambda * Theta)

    # unravel the gradient matrices into a single array
    grad = np.concatenate((np.ravel(X_grad), np.ravel(Theta_grad)))
    
    return J, grad
    
# load up our data
data = loadmat('data/ex8_movies.mat')

# Y = number of movies x number of users, R is the indicator array indicating if a user has seen a particular movie
Y = data['Y']
R = data['R']

# mean rating
print('\nMean film rating: {0:.2f}'.format(Y[1,R[1,:]].mean()))

# attempt to visualise the data
fig, ax = plt.subplots(figsize=(12,12))
ax.imshow(Y)
ax.set_xlabel('Users')
ax.set_ylabel('Movies')
fig.tight_layout()
plt.show()

# use pretrained parameters to test sub-set of data
num_users = 4
num_movies = 5
num_features = 3

params_data = loadmat('data/ex8_movieParams.mat')
X = params_data['X']
Theta = params_data['Theta']

X_sub = X[:num_movies, :num_features]
Theta_sub = Theta[:num_users, :num_features]
Y_sub = Y[:num_movies, :num_users]
R_sub = R[:num_movies, :num_users]

params = np.concatenate((np.ravel(X_sub), np.ravel(Theta_sub)))

print('\nWe get the following cost for the pretrained parameters: {0:.4f}'.format(costFunction(params, Y_sub, R_sub, num_features, 1)[0]))

# link movie index to its title
movie_idx = {}
f = open('data/movie_ids.txt', encoding="ISO-8859-1")
for line in f:
    tokens = line.split(' ')
    tokens[-1] = tokens[-1][:-1]
    movie_idx[int(tokens[0]) - 1] = ' '.join(tokens[1:])

# train collaborative filtering model
num_movies = Y.shape[0]
num_users = Y.shape[1]
num_features = 10
learning_rate = 10

# randomly intiialize parameters
X = np.random.random(size=(num_movies, num_features))
Theta = np.random.random(size=(num_users, num_features))
params = np.concatenate((np.ravel(X), np.ravel(Theta)))

Ymean = np.zeros((num_movies, 1))
Ynorm = np.zeros((num_movies, num_users))

for i in range(num_movies):
    idx = np.where(R[i,:] == 1)[0]
    Ymean[i] = Y[i,idx].mean()
    Ynorm[i, idx] = Y[i, idx] - Ymean[i]

# train model
fmin = minimize(fun=costFunction, x0=params, args=(Ynorm, R, num_features, learning_rate), method='CG', jac=True, options={'maxiter': 100})

# reshape matrices back to original dimensions
X = np.matrix(np.reshape(fmin.x[:num_movies * num_features], (num_movies, num_features)))
Theta = np.matrix(np.reshape(fmin.x[num_movies * num_features:], (num_users, num_features)))

# can predict some ratings for last user
predictions = X * Theta.T
my_preds = predictions[:, -1] + Ymean
sorted_preds = np.sort(my_preds, axis=0)[::-1]
sorted_preds[:10]

# find what index those ratings are for
idx = np.argsort(my_preds, axis=0)[::-1]
print("\nTop 10 movie predictions:")
for i in range(10):
    j = int(idx[i])
    print('Predicted rating of {0:.2f} for movie {1}.'.format(float(my_preds[j]),movie_idx[j]))
