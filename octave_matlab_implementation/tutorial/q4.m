function z = q4()

  v = [ 7 6 5 4 3 2 1 ];
  w = [ 1 2 3 4 5 6 7 ];

  z = 0;
  for i = 1:7
      z = z + v(i) * w(i);
  end
  
  disp(z);

  disp(sum(v.*w));
  disp(w'*v);
  %disp(v*w);
  %disp(w*v);

end
