function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

param_vec = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];

error_train = zeros(length(param_vec), length(param_vec));

for i = 1:length(param_vec)
  C = param_vec(i);

  for j = 1:length(param_vec)
    sigma = param_vec(j);

    % find whatever we're looking for
    model       = svmTrain(X, y, C, @(x1, x2) gaussianKernel(x1, x2, sigma));
    predictions = svmPredict(model, Xval);

    error(i, j) = mean(double(predictions ~= yval));

  endfor

endfor

error_min = min(min(error));

[C_index, sigma_index] = find(error == error_min);

C     = param_vec(C_index);
sigma = param_vec(sigma_index);

fprintf('\nValues for C and sigma: %f and %f with prediction error %f.',
	C, sigma, error_min);

% =========================================================================

end
